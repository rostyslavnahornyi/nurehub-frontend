declare module "*.svg" {
  const content: React.FC<React.SVGProps<SVGElement>>;
  export default content;
}
declare module "*.scss" {
  const content: Record<string, string>;
  export default content;
}
declare module "*.jpg";
declare module "*.png";
declare module "*.jpeg";
declare module "*.gif";

declare interface MetaEnv {
  readonly VITE_PRIVATE_API: string;
  readonly VITE_PUBLIC_API: string;
}

interface ImportMeta {
  readonly env: MetaEnv;
}

/// <reference types="vite/client" />
