/**
 * Adding route here - add the same route into src/app/router.tsx
 */
const ROUTES = {
  ROOT: "/",
  APP: {
    ROOT: "/app",
    AUTH: {
      ROOT: "/app/auth",
      SIGNUP: "/app/auth/signup",
      LOGIN: "/app/auth/login",
      RESET_PASSWORD: "/app/auth/reset-password",
      REQUEST_INVITE: "/app/auth/request-invite",
    },
  },
};

export { ROUTES };
