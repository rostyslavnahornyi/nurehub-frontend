import i18n from "i18next";
import { initReactI18next } from "react-i18next";

import NS_EN_COMMON from "./locales/en/common.en.json";
import NS_EN_COMPONENTS from "./locales/en/components.en.json";
import NS_EN_PAGES from "./locales/en/pages.en.json";

import NS_UA_COMMON from "./locales/ua/common.ua.json";
import NS_UA_COMPONENTS from "./locales/ua/components.ua.json";
import NS_UA_PAGES from "./locales/ua/pages.ua.json";

const resources = {
  en: {
    common: NS_EN_COMMON,
    pages: NS_EN_PAGES,
    components: NS_EN_COMPONENTS,
  },
  ua: {
    common: NS_UA_COMMON,
    pages: NS_UA_PAGES,
    components: NS_UA_COMPONENTS,
  },
} as const;

i18n.use(initReactI18next).init({
  ns: ["pages", "common", "components"],
  resources,
  fallbackLng: "ua",
  interpolation: {
    escapeValue: false,
  },
  react: {
    useSuspense: false,
  },
});

export { i18n, resources };
