const { VITE_PRIVATE_API, VITE_PUBLIC_API } = import.meta.env;

const ENV: MetaEnv = {
  VITE_PRIVATE_API,
  VITE_PUBLIC_API,
};

export { ENV };
