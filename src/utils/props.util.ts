interface ElementStyle {
  style?: React.CSSProperties;
  className?: string;
}

export type { ElementStyle };
