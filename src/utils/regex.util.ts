const SPECIAL_CHAR_REGEX = /[!@#$%^&*(),.?":{}|<>]/;
const DIGIT_REGEX = /\d/;
const UPPERCASE_REGEX = /[A-Z]/;

export { DIGIT_REGEX, SPECIAL_CHAR_REGEX, UPPERCASE_REGEX };
