import { ModalData, ModalIDs } from "@components/common/modals/components";
import { ModalMeta } from "@components/common/modals/modals.types";
import { create } from "zustand";
import { devtools } from "zustand/middleware";
import { immer } from "zustand/middleware/immer";

type AUIState = {
  modals: { id: ModalIDs; data: ModalData[ModalIDs] }[];
};

type AUIActions = {
  actions: {
    addModal: <T extends ModalIDs>(modalMeta: ModalMeta<T>) => void;
    removeModal: (id: ModalIDs) => void;
  };
};

const defaultAUIValues: AUIState = {
  modals: [],
};

const useAUIStore = create<AUIState & AUIActions>()(
  devtools(
    immer((set) => ({
      ...defaultAUIValues,
      actions: {
        addModal: (modalMeta) =>
          set((state) => {
            if (state.modals.some(({ id }) => id === modalMeta.id)) {
              return;
            }
            state.modals.push(modalMeta);
          }),

        removeModal: (id) =>
          set((state) => {
            const modalIndex = state.modals.findIndex(
              ({ id: ModalIDs }) => ModalIDs === id
            );
            if (modalIndex !== -1) {
              state.modals.splice(modalIndex, 1);
            }
          }),
      },
    }))
  )
);

export { useAUIStore };
