type RoleType = "Admin" | "Member" | "Guest";

export type { RoleType };
