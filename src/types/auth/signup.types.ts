type SignupRequest = {
  email: string;
  password: string;
  groupId?: string;
  positions?: string[];
};

export type { SignupRequest };
