import { publicRequest } from "@api";

type ResetPasswordBody = {
  email: string;
  password: string;
  confirmPassword: string;
};

type ResetPasswordResponse = object;

export const ResetPassword = (body: ResetPasswordBody) =>
  publicRequest.post<ResetPasswordResponse>("auth/ResetPassword", body);
