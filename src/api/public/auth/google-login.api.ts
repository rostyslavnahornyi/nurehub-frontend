import { publicRequest } from "@api";

type GoogleLoginBody = {
  token: string;
};

type GoogleLoginResponse = {
  accessToken: string;
};

export const GoogleLogin = (body: GoogleLoginBody) =>
  publicRequest.post<GoogleLoginResponse>("auth/GoogleLogin", { params: body });
