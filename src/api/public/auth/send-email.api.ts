import { publicRequest } from "@api";

type SendEmailBody = {
  token: string;
};

type SendEmailResponse = {
  accessToken: string;
};

export const sendEmail = (body: SendEmailBody) =>
  publicRequest.post<SendEmailResponse>("auth/SendEmail", {
    params: body,
  });
