import { publicRequest } from "@api";

type LoginBody = {
  email: string;
  password: string;
};

type LoginResponse = {
  accessToken: string;
};

export const login = (body: LoginBody) =>
  publicRequest.post<LoginResponse>("auth/Login", body);
