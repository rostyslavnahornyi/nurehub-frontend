import { publicRequest } from "@api";

type VerifyCodeBody = {
  email: string;
  code: string;
};

type VerifyCodeResponse = {
  accessToken: string;
};

export const VerifyCode = (body: VerifyCodeBody) =>
  publicRequest.post<VerifyCodeResponse>("auth/VerifyCode", body);
