import { publicRequest } from "@api";

type Check2FABody = {
  email: string;
};

type Check2FAResponse = {
  accessToken: string;
};

export const check2FA = (body: Check2FABody) =>
  publicRequest.get<Check2FAResponse>("auth/CheckTwoFA", { params: body });
