import { publicRequest } from "@api";

type SendRequestBody = {
  email: string;
  reason: string;
};

type SendRequestResponse = object;

export const SendRequest = (body: SendRequestBody) =>
  publicRequest.post<SendRequestResponse>("auth/SendRequest", body);
