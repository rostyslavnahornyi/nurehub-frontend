import { publicRequest } from "@api";

type LogoutResponse = object;

export const Logout = () => publicRequest.post<LogoutResponse>("auth/Logout");
