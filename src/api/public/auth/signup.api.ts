import { publicRequest } from "@api";
import { RoleType } from "@types";

type SignUpBody = {
  name: string;
  email: string;
  password: string;
  groupId?: string;
  positions?: string;
  roleType?: RoleType;
};

type SignUpResponse = {
  accessToken: string;
};

export const signUp = ({ roleType = "Member", ...body }: SignUpBody) =>
  publicRequest.post<SignUpResponse>("auth/signUp", { ...body, roleType });
