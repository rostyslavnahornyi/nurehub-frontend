import { useAUIStore } from "@stores";
import { ENV } from "@utils";
import axios, { AxiosError, AxiosRequestConfig } from "axios";

const axiosBaseConfig: AxiosRequestConfig = {
  timeout: 60 * 1000,
  headers: {
    "Content-Type": "application/json",
    Accept: "application/json",
  },
};

const publicRequest = axios.create({
  ...axiosBaseConfig,
  baseURL: ENV.VITE_PUBLIC_API,
});

const privateRequest = axios.create({
  ...axiosBaseConfig,
  baseURL: ENV.VITE_PRIVATE_API,
});

const callConnectionErrorModal = () =>
  useAUIStore.getState().actions.addModal({ id: "CONNECTION_ERROR", data: {} });
const callServerErrorModal = () =>
  useAUIStore.getState().actions.addModal({ id: "SERVER_ERROR", data: {} });

const onError = (e: AxiosError) => {
  console.log(e);
  if (e.response) {
    callServerErrorModal();
  } else if (e.request) {
    callConnectionErrorModal();
  }
};

publicRequest.interceptors.response.use(null, onError);
privateRequest.interceptors.response.use(null, onError);

export { privateRequest, publicRequest };
