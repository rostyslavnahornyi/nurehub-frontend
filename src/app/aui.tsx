/* eslint-disable react-refresh/only-export-components */

import { Modals } from "@components/common";
import { FC } from "react";

const AUI: FC = () => (
  <>
    <Modals />
  </>
);

export { AUI };
