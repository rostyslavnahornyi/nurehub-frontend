import { Providers, Router } from "@app";
import { FC } from "react";

import "@styles/core.scss";

const App: FC = () => (
  <Providers>
    <Router />
  </Providers>
);

export { App };
