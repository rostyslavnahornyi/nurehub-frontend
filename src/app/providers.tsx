import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { i18n } from "@utils/i18n";
import { FC, PropsWithChildren } from "react";
import { I18nextProvider } from "react-i18next";
import { AUI } from "./aui";

const queryClient = new QueryClient();

const Providers: FC<PropsWithChildren> = ({ children }) => (
  <I18nextProvider i18n={i18n}>
    <QueryClientProvider client={queryClient}>
      <AUI />
      {children}
    </QueryClientProvider>
  </I18nextProvider>
);

export { Providers };
