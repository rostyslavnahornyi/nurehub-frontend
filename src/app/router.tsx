import { AuthLayout, MainPage, NotFoundPage, SignupPage } from "@pages";
import { FC } from "react";
import {
  RouterProvider as ReactRouterProvider,
  createBrowserRouter,
} from "react-router-dom";

/**
 * Adding route here - add the same route into src/utils/routes.ts
 */
const router = createBrowserRouter([
  {
    path: "/",
    Component: MainPage,
  },
  {
    path: "/app",
    children: [
      {
        path: "auth",
        Component: AuthLayout,
        children: [
          { path: "signup", Component: SignupPage },
          { path: "login", element: <div>login</div> },
          { path: "reset-password", element: <div>reset pass</div> },
          { path: "request-invite", element: <div>request-invite</div> },
        ],
      },
    ],
  },
  {
    path: "*",
    Component: NotFoundPage,
  },
]);

const Router: FC = () => <ReactRouterProvider router={router} />;

export { Router };
