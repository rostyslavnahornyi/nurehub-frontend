import { lazy } from "react";

const AuthLayout = lazy(() => import("./auth.layout"));

export { AuthLayout };
