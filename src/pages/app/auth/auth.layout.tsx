import { Logo, Typography } from "@components/ui-kit";
import { FC } from "react";
import { Outlet } from "react-router-dom";
import { useAuth } from "./auth.hook";
import styles from "./auth.module.scss";

const Auth: FC = () => {
  const { t } = useAuth();

  return (
    <main className={styles.authLayoutWrapper}>
      <div className={styles.company}>
        <Logo variant="dark" />
        <Typography.Text variant="h6">{t("nurehub")}</Typography.Text>
      </div>
      <Outlet />
    </main>
  );
};

export default Auth;
