import { useTranslation } from "react-i18next";

const useAuth = () => {
  const { t } = useTranslation("common");

  return { t };
};

export { useAuth };
