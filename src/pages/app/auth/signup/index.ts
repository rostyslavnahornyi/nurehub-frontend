import { lazy } from "react";

const SignupPage = lazy(() => import("./signup.page"));

export { SignupPage };
