import { AuthForm } from "@components/common";
import { FC } from "react";

const Signup: FC = () => {
  return <AuthForm.Signup />;
};

export default Signup;
