import { lazy } from "react";

const MainPage = lazy(() => import("./main.page"));

export { MainPage };
