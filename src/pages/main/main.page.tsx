import { Typography } from "@components/ui-kit";
import { FC } from "react";

const Main: FC = () => {
  return (
    <>
      <Typography.Text variant="h1">MAIN PAGE</Typography.Text>
    </>
  );
};

export default Main;
