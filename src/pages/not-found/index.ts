import { lazy } from "react";

const NotFoundPage = lazy(() => import("./not-found.page"));

export { NotFoundPage };
