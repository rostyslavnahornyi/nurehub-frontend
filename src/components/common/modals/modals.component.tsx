import { useAUIStore } from "@stores";
import { FC } from "react";
import { ModalData, ModalIDs, modalComponents } from "./components";

const Modals: FC = () => {
  const modals = useAUIStore((store) => store.modals);

  return modals.map(({ id, data }) => {
    const ModalComponent = modalComponents[id] as FC<ModalData[ModalIDs]>;

    return <ModalComponent key={id} {...data} />;
  });
};

export { Modals };
