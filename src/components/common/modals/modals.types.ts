import { ModalData, ModalIDs } from "./components";

type ModalMeta<T extends ModalIDs> = {
  id: T;
  data: ModalData[T];
};

export type { ModalMeta };
