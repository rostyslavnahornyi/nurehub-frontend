import { useAUIStore } from "@stores";
import { useTranslation } from "react-i18next";

const useConnectionErrorModal = () => {
  const { t } = useTranslation("components", {
    keyPrefix: "common.modals.connection-error",
  });
  const { removeModal } = useAUIStore((state) => state.actions);

  const onClose = () => removeModal("CONNECTION_ERROR");

  return { t, onClose };
};

export { useConnectionErrorModal };
