import HeartbeatGif from "@assets/images/heartbeat.gif";
import { Modal, Typography } from "@components/ui-kit";
import { FC } from "react";
import { useConnectionErrorModal } from "./connection-error-modal.hook";
import styles from "./connection-error-modal.module.scss";
import { ConnectionErrorModalProps } from "./connection-error-modal.types";

const CONNECTION_ERROR_MODAL_ID = "CONNECTION_ERROR";

const ConnectionErrorModal: FC<ConnectionErrorModalProps> = () => {
  const { t, onClose } = useConnectionErrorModal();

  return (
    <Modal
      id={CONNECTION_ERROR_MODAL_ID}
      title={t("title")}
      size="small"
      controlButtons={{
        primary: { variant: "grey", children: t("btnLabel"), onClick: onClose },
      }}
      className={styles.modalWrapper}
      onClose={onClose}
    >
      <img alt="heartbeat" src={HeartbeatGif} />
      <Typography.Text variant="body2">{t("description")}</Typography.Text>
    </Modal>
  );
};

export { CONNECTION_ERROR_MODAL_ID, ConnectionErrorModal };
