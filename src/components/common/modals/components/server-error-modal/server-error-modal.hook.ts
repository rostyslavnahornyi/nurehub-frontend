import { useAUIStore } from "@stores";
import { useTranslation } from "react-i18next";

const useServerErrorModal = () => {
  const { t } = useTranslation("components", {
    keyPrefix: "common.modals.server-error",
  });
  const { removeModal } = useAUIStore((state) => state.actions);

  const onClose = () => removeModal("SERVER_ERROR");

  return { t, onClose };
};

export { useServerErrorModal };
