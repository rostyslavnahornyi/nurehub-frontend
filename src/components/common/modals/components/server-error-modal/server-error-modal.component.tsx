import HeartbeatGif from "@assets/images/heartbeat.gif";
import { Modal, Typography } from "@components/ui-kit";
import { FC } from "react";
import { useServerErrorModal } from "./server-error-modal.hook";
import styles from "./server-error-modal.module.scss";
import { ServerErrorModalProps } from "./server-error-modal.types";

const SERVER_ERROR_MODAL_ID = "SERVER_ERROR";

const ServerErrorModal: FC<ServerErrorModalProps> = () => {
  const { t, onClose } = useServerErrorModal();

  return (
    <Modal
      id={SERVER_ERROR_MODAL_ID}
      title={t("title")}
      size="small"
      controlButtons={{
        primary: { variant: "grey", children: t("btnLabel"), onClick: onClose },
      }}
      className={styles.modalWrapper}
      onClose={onClose}
    >
      <img alt="heartbeat" src={HeartbeatGif} />
      <Typography.Text variant="body2">{t("description")}</Typography.Text>
    </Modal>
  );
};

export { SERVER_ERROR_MODAL_ID, ServerErrorModal };
