import {
  CONNECTION_ERROR_MODAL_ID,
  ConnectionErrorModal,
  ConnectionErrorModalProps,
} from "./connection-error-modal";
import {
  SERVER_ERROR_MODAL_ID,
  ServerErrorModal,
  ServerErrorModalProps,
} from "./server-error-modal";

/**
 * @label ID
 * @description List of modal IDs
 */
enum ModalIDsEnum {
  CONNECTION_ERROR = CONNECTION_ERROR_MODAL_ID,
  SERVER_ERROR = SERVER_ERROR_MODAL_ID,
}

/**
 * @label Props
 * @description List of props to each modal with ID
 */
type ModalData = {
  CONNECTION_ERROR: ConnectionErrorModalProps;
  SERVER_ERROR: ServerErrorModalProps;
};

/**
 * @label Components
 * @description List of modal components
 */
const modalComponents: Record<ModalIDs, unknown> = {
  CONNECTION_ERROR: ConnectionErrorModal,
  SERVER_ERROR: ServerErrorModal,
};

type ModalIDs = keyof typeof ModalIDsEnum;

export { modalComponents };
export type { ModalData, ModalIDs };
