import { Login, RequestInvite, Signup } from "./variants";

const AuthForm = {
  Login,
  Signup,
  RequestInvite,
};

export { AuthForm };
