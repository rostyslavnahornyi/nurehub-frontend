import { useTranslation } from "react-i18next";

const useAuthFormBase = () => {
  const { t } = useTranslation("components", {
    keyPrefix: "common.auth-form.components.authFormBase.banner",
  });

  return { t };
};

export { useAuthFormBase };
