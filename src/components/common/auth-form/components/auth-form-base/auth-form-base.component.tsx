import PersonImg from "@assets/images/om-avatar.jpg";
import { Avatar, Typography } from "@components/ui-kit";
import classNames from "classnames";
import { FC } from "react";
import { useAuthFormBase } from "./auth-form-base.hook";
import styles from "./auth-form-base.module.scss";
import { AuthFormBaseProps } from "./auth-form-base.types";

const AuthFormBase: FC<AuthFormBaseProps> = ({
  title,
  description,
  banner = true,
  children,
}) => {
  const { t } = useAuthFormBase();

  return (
    <div className={styles.authFormWrapper}>
      <div
        className={classNames(
          styles.formWrapper,
          !banner && styles.withoutBanner
        )}
      >
        <div className={styles.header}>
          <Typography.Text variant="h4">{title}</Typography.Text>
          {description && (
            <Typography.Text variant="body2">{description}</Typography.Text>
          )}
        </div>

        <div className={styles.innerForm}>{children}</div>
      </div>

      {banner && (
        <div className={styles.banner}>
          <Typography.Text variant="body2" className={styles.text}>
            {t("text")}
          </Typography.Text>

          <div className={styles.authorBlock}>
            <Avatar.Round size="m" src={PersonImg} />

            <div className={styles.description}>
              <Typography.Text variant="body1">
                {t("person.name")}
              </Typography.Text>

              <Typography.Text variant="body1">
                {t("person.position")}
              </Typography.Text>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export { AuthFormBase };
