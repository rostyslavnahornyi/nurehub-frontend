import { PropsWithChildren } from "react";

interface AuthFormBaseProps extends PropsWithChildren {
  title: string;
  description?: string;
  banner?: boolean;
}

export type { AuthFormBaseProps };
