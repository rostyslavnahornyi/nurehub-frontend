import { FC } from "react";
import { AuthFormBase } from "../../components";
import { useSignup } from "./signup.hook";

const Signup: FC = () => {
  const { enableBanner, headerTranslations, currentStepElement } = useSignup();

  return (
    <AuthFormBase {...headerTranslations} banner={enableBanner}>
      {currentStepElement}
    </AuthFormBase>
  );
};

export { Signup };
