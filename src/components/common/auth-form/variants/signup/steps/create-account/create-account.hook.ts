import { DIGIT_REGEX, SPECIAL_CHAR_REGEX, UPPERCASE_REGEX } from "@utils";
import { useState } from "react";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import * as yup from "yup";
import {
  CreateAccountProps,
  CreateAccountScheme,
} from "./create-account.types";

const formInitialValues: CreateAccountScheme = {
  email: "",
  password: "",
};

const useCreateAccountStep = ({
  onNextStep,
  setSignupValues,
}: CreateAccountProps) => {
  const navigate = useNavigate();
  const { t } = useTranslation("components", {
    keyPrefix: "common.auth-form.variants.signup.steps.create-account.form",
  });

  const [isPasswordHidden, setIsPasswordHidden] = useState(true);

  const onSubmit = (values: CreateAccountScheme) => {
    setSignupValues(values);
    onNextStep();
  };

  const togglePasswordVisibility = () => setIsPasswordHidden((prev) => !prev);

  const formValidationSchema: yup.ObjectSchema<CreateAccountScheme> = yup
    .object()
    .shape({
      email: yup
        .string()
        .required(t("inputs.email.errors.empty"))
        .email(t("inputs.email.errors.email"))
        .test(
          "nureDomain",
          t("inputs.email.errors.nureDomain"),
          function (value) {
            if (value.endsWith("@nure.ua")) {
              return true;
            }
            return false;
          }
        ),
      password: yup
        .string()
        .required(t("inputs.password.errors.empty"))
        .min(6, t("inputs.password.errors.min"))
        .test(
          "specialChar",
          t("inputs.password.errors.specialChar"),
          function (value) {
            return SPECIAL_CHAR_REGEX.test(value);
          }
        )
        .test("digit", t("inputs.password.errors.digit"), function (value) {
          return DIGIT_REGEX.test(value);
        })
        .test(
          "uppercase",
          t("inputs.password.errors.uppercase"),
          function (value) {
            return UPPERCASE_REGEX.test(value);
          }
        ),
    });

  return {
    t,
    formInitialValues,
    formValidationSchema,
    isPasswordHidden,
    navigate,
    onSubmit,
    togglePasswordVisibility,
  };
};

export { useCreateAccountStep };
