import { SignupRequest } from "@types";

type CreateAccountScheme = Pick<SignupRequest, "email" | "password">;

type CreateAccountProps = {
  onNextStep: () => void;
  setSignupValues: (values: CreateAccountScheme) => void;
};


export type { CreateAccountProps, CreateAccountScheme };
