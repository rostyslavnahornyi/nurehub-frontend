import ArrowRightIcon from "@assets/icons/arrow-right.svg";
import ClosedEyeIcon from "@assets/icons/eye-off.svg";
import OpenedEyeIcon from "@assets/icons/eye.svg";
import GoogleIcon from "@assets/icons/google.svg";
import { Button, Divider, Input, Steps, Typography } from "@components/ui-kit";
import { ROUTES } from "@utils";
import { Form, Formik } from "formik";
import { FC } from "react";
import { useCreateAccountStep } from "./create-account.hook";
import styles from "./create-account.module.scss";
import { CreateAccountProps } from "./create-account.types";

const CreateAccountStep: FC<CreateAccountProps> = (props) => {
  const {
    t,
    formInitialValues,
    formValidationSchema,
    isPasswordHidden,
    navigate,
    onSubmit,
    togglePasswordVisibility,
  } = useCreateAccountStep(props);

  return (
    <div className={styles.createAccountStepWrapper}>
      <Button
        className={styles.googleBtn}
        size="large"
        variant="borders"
        ignoreSvgColor
        leadingIcon={<GoogleIcon />}
      >
        {t("signInGoogle")}
      </Button>

      <Divider
        textProps={{
          variant: "body1",
          children: t("dividerText"),
          className: styles.dividerText,
        }}
      />

      <Formik
        initialValues={formInitialValues}
        validationSchema={formValidationSchema}
        onSubmit={onSubmit}
      >
        {({ touched, errors, setFieldValue, setFieldTouched }) => (
          <Form className={styles.formWrapper}>
            <div className={styles.inputs}>
              <Input.Default
                title={t("inputs.email.title")}
                fullWidth
                size="large"
                error={touched.email && !!errors.email}
                validationText={touched.email ? errors.email : undefined}
                placeholder={t("inputs.email.placeholder")}
                onChange={(value) => setFieldValue("email", value)}
                onFocus={() => setFieldTouched("email", true)}
              />
              <Input.Default
                title={t("inputs.password.title")}
                fullWidth
                size="large"
                error={touched.password && !!errors.password}
                validationText={touched.password ? errors.password : undefined}
                placeholder={t("inputs.password.placeholder")}
                type={isPasswordHidden ? "password" : "text"}
                trailingIcon={
                  isPasswordHidden ? (
                    <ClosedEyeIcon onClick={togglePasswordVisibility} />
                  ) : (
                    <OpenedEyeIcon onClick={togglePasswordVisibility} />
                  )
                }
                onFocus={() => setFieldTouched("password", true)}
                onChange={(value) => setFieldValue("password", value)}
              />
            </div>

            <div className={styles.controls}>
              <div className={styles.questions}>
                <Typography.Text
                  variant="link1"
                  onClick={() => navigate(ROUTES.APP.AUTH.REQUEST_INVITE)}
                >
                  {t("questions.0")}
                </Typography.Text>
                <Typography.Text
                  variant="link1"
                  onClick={() => navigate(ROUTES.APP.AUTH.RESET_PASSWORD)}
                >
                  {t("questions.1")}
                </Typography.Text>
              </div>

              <Button
                htmlType="submit"
                fullWidth
                size="large"
                variant="primary"
                trailingIcon={
                  <ArrowRightIcon className={styles.rightArrowIcon} />
                }
              >
                {t("buttons.0")}
              </Button>
              <Button
                fullWidth
                size="large"
                variant="secondary"
                onClick={() => navigate(ROUTES.APP.AUTH.LOGIN)}
              >
                {t("buttons.1")}
              </Button>

              <Steps className={styles.steps} count={2} currentIdx={1} />
            </div>
          </Form>
        )}
      </Formik>
    </div>
  );
};

export { CreateAccountStep };
