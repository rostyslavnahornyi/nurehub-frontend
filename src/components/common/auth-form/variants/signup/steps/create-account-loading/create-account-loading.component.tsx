import { Loader } from "@components/ui-kit";
import { FC, useEffect } from "react";
import styles from "./create-account-loading.module.scss";
import { CreateAccountLoadingProps } from "./create-account-loading.types";

const CreateAccountLoadingStep: FC<CreateAccountLoadingProps> = ({
  onNextStep,
}) => {
  useEffect(() => {
    setTimeout(() => {
      onNextStep();
    }, 2000);
  }, []);

  return (
    <div className={styles.createAccountLoadingWrapper}>
      <Loader size="large" />
    </div>
  );
};

export { CreateAccountLoadingStep };
