type CreateAccountLoadingProps = {
  onNextStep: () => void;
};

export type { CreateAccountLoadingProps };
