import { Loader } from "@components/ui-kit";
import { FC, useEffect } from "react";
import styles from "./details-loading.module.scss";
import { DetailsLoadingStepProps } from "./details-loading.types";

const DetailsLoadingStep: FC<DetailsLoadingStepProps> = ({ onNextStep }) => {
  useEffect(() => {
    setTimeout(() => {
      onNextStep();
    }, 2000);
  }, []);

  return (
    <div className={styles.detailsLoadingStepWrapper}>
      <Loader size="large" />
    </div>
  );
};

export { DetailsLoadingStep };
