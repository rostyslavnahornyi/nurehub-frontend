type DetailsLoadingStepProps = {
  onNextStep: () => void;
};

export type { DetailsLoadingStepProps };
