import SuccessGif from "@assets/images/success-circle.gif";
import { Button } from "@components/ui-kit";
import { FC } from "react";
import { useSuccessStep } from "./success.hook";
import styles from './success.module.scss'

const SuccessStep: FC = () => {
  const { t } = useSuccessStep();

  return (
    <div className={styles.successStepWrapper}>
      <img src={SuccessGif} alt={"success-gif"} />

      <Button size="medium" variant="primary" fullWidth>
        {t("buttonLabel")}
      </Button>
    </div>
  );
};

export { SuccessStep };
