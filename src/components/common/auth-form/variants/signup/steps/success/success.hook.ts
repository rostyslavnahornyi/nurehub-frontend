import { useTranslation } from "react-i18next";

const useSuccessStep = () => {
  const { t } = useTranslation("components", {
    keyPrefix: "common.auth-form.variants.signup.steps.success",
  });

  return { t };
};

export { useSuccessStep };
