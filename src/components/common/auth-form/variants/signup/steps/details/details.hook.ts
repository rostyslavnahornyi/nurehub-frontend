import { useTranslation } from "react-i18next";
import * as yup from "yup";
import { DetailsStepProps, DetailsStepScheme } from "./details.types";

const formInitialValues: DetailsStepScheme = {};

const useDetailsStep = ({ onNextStep, setSignupValues }: DetailsStepProps) => {
  const { t } = useTranslation("components", {
    keyPrefix: "common.auth-form.variants.signup.steps.details.form",
  });

  const onSubmit = (values: DetailsStepScheme) => {
    setSignupValues(values);
    onNextStep();
  };

  const formValidationSchema: yup.ObjectSchema<DetailsStepScheme> = yup
    .object()
    .shape({
      groupId: yup.string().optional(),
      positions: yup.array().of(yup.string().required()).optional(),
    })
    .test("path", "message", ({ groupId, positions }, { createError }) => {
      if (!!groupId && !!positions) {
        return createError({
          path: "allFieldsExist",
          message: "only one field must be set",
        });
      }

      if (!groupId && !positions) {
        return createError({
          path: "noOneFieldExists",
          message: "one field must be set",
        });
      }

      return true;
    });

  return {
    t,
    formInitialValues,
    formValidationSchema,
    onSubmit,
  };
};

export { useDetailsStep };
