import { Button, Divider, Input, Steps, Toast } from "@components/ui-kit";
import { Form, Formik } from "formik";
import { FC } from "react";
import { useDetailsStep } from "./details.hook";
import styles from "./details.module.scss";
import { DetailsStepProps } from "./details.types";

const DetailsStep: FC<DetailsStepProps> = ({ onNextStep, setSignupValues }) => {
  const { t, formInitialValues, formValidationSchema, onSubmit } =
    useDetailsStep({ onNextStep, setSignupValues });

  return (
    <Formik
      initialValues={formInitialValues}
      validationSchema={formValidationSchema}
      onSubmit={onSubmit}
    >
      {({ errors, submitCount, setFieldValue }) => (
        <Form className={styles.detailsStepWrapper}>
          {submitCount && Object.keys(errors).length ? (
            <Toast
              focusType="high"
              size="small"
              variant="error"
              title={t(`errors.${Object.keys(errors)[0]}`)}
            />
          ) : null}
          <div className={styles.inputs}>
            <Input.Select
              title={t("inputs.group.label")}
              placeholder={t("inputs.group.placeholder")}
              onChange={(value) => setFieldValue("groupId", value)}
              options={[
                { label: "PZPI-20-3", value: "pzpi-20-3" },
                { label: "AAA-20-3", value: "aaa-20-3" },
                { label: "BBB-20-3", value: "bbb-20-3" },
              ]}
              searchable
              clearable
              fullWidth
            />

            <Divider
              textProps={{
                variant: "body1",
                children: t("dividerText"),
                className: styles.dividerText,
              }}
            />

            <Input.Tag
              title={t("inputs.positions.label")}
              secondaryTitle={t("inputs.positions.hint")}
              placeholder={t("inputs.positions.placeholder")}
              onChange={(values) => setFieldValue("positions", values)}
            />
          </div>

          <div className={styles.footer}>
            <Button size="medium" variant="primary" htmlType="submit">
              {t("submitBtn")}
            </Button>

            <Steps className={styles.steps} count={2} currentIdx={2} />
          </div>
        </Form>
      )}
    </Formik>
  );
};

export { DetailsStep };
