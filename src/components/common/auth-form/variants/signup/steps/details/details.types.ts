import { SignupRequest } from "@types";

type DetailsStepScheme = Pick<SignupRequest, "groupId" | "positions">;

type DetailsStepProps = {
  onNextStep: () => void;
  setSignupValues: (values: DetailsStepScheme) => void;
};


export type { DetailsStepProps, DetailsStepScheme };
