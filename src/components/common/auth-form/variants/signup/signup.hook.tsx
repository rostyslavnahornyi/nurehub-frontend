import { SignupRequest } from "@types";
import { i18n } from "@utils";
import { Dispatch, SetStateAction, useState } from "react";
import { AuthFormBaseProps } from "../../components/auth-form-base/auth-form-base.types";
import { SignupStep } from "./signup.types";
import {
  CreateAccountLoadingStep,
  CreateAccountStep,
  DetailsLoadingStep,
  DetailsStep,
  SuccessStep,
} from "./steps";

const t = (postfix: string) =>
  i18n.t(`components:common.auth-form.variants.signup.steps.${postfix}`);

const HEADER_TRANSLATIONS: Record<
  SignupStep,
  Pick<AuthFormBaseProps, "title" | "description">
> = {
  CREATE_ACCOUNT: {
    title: t("create-account.header.title"),
    description: t("create-account.header.description"),
  },
  CREATE_ACCOUNT_LOADING: {
    title: t("create-account-loading.header.title"),
  },
  DETAILS: {
    title: t("details.header.title"),
    description: t("details.header.description"),
  },
  DETAILS_LOADING: {
    title: t("details-loading.header.title"),
  },
  SUCCESS: {
    title: t("success.header.title"),
    description: t("success.header.description"),
  },
};

const renderStepElement: ({
  setCurrentStep,
  setSignupValues,
}: {
  setCurrentStep: Dispatch<SetStateAction<SignupStep>>;
  setSignupValues: Dispatch<SetStateAction<SignupRequest>>;
}) => Record<SignupStep, JSX.Element> = ({
  setCurrentStep,
  setSignupValues,
}) => ({
  CREATE_ACCOUNT: (
    <CreateAccountStep
      onNextStep={() => setCurrentStep(SignupStep.CREATE_ACCOUNT_LOADING)}
      setSignupValues={(signupStepValues) =>
        setSignupValues((values) => ({ ...values, ...signupStepValues }))
      }
    />
  ),
  CREATE_ACCOUNT_LOADING: (
    <CreateAccountLoadingStep
      onNextStep={() => setCurrentStep(SignupStep.DETAILS)}
    />
  ),
  DETAILS: (
    <DetailsStep
      onNextStep={() => setCurrentStep(SignupStep.DETAILS_LOADING)}
      setSignupValues={(detailsStepValues) =>
        setSignupValues((values) => ({ ...values, ...detailsStepValues }))
      }
    />
  ),
  DETAILS_LOADING: (
    <DetailsLoadingStep onNextStep={() => setCurrentStep(SignupStep.SUCCESS)} />
  ),
  SUCCESS: <SuccessStep />,
});

const useSignup = () => {
  const [currentStep, setCurrentStep] = useState<SignupStep>(
    SignupStep.CREATE_ACCOUNT
  );
  const [signupValues, setSignupValues] = useState<SignupRequest>({
    email: "",
    password: "",
  });
  console.log({ signupValues });

  const headerTranslations = HEADER_TRANSLATIONS[currentStep];
  const currentStepElement = renderStepElement({
    setCurrentStep,
    setSignupValues,
  })[currentStep];

  const enableBanner = currentStep !== SignupStep.SUCCESS;

  return {
    enableBanner,
    headerTranslations,
    currentStepElement,
  };
};

export { useSignup };
