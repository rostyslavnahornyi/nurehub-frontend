import { PropsWithChildren } from "react";

interface PortalProps extends PropsWithChildren {
  id: string;
}

export type { PortalProps };
