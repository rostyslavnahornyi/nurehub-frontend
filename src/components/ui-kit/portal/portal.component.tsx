import { FC, useEffect, useState } from "react";
import { createPortal } from "react-dom";
import { PortalProps } from "./portal.types";

const Portal: FC<PortalProps> = ({ id, children }) => {
  const [container, setContainer] = useState<HTMLElement>();

  useEffect(() => {
    if (document.getElementById(id)) {
      return;
    }

    const portalContainer = document.createElement("div");
    portalContainer.setAttribute("id", id);
    document.body.appendChild(portalContainer);

    setContainer(portalContainer);

    return () => {
      document.getElementById(id)?.remove();
    };
  }, []);

  return container ? createPortal(children, container) : null;
};

export { Portal };
