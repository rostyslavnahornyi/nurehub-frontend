import { Typography } from "@components/ui-kit";
import classNames from "classnames";
import { FC } from "react";
import styles from "./button.module.scss";
import { ButtonProps } from "./button.types";

const Button: FC<ButtonProps> = ({
  variant,
  size,
  children,
  fullWidth = false,
  selected = false,
  disabled = false,
  destructive = false,
  ignoreSvgColor = false,
  leadingIcon,
  trailingIcon,
  htmlType = "button",
  style,
  className,
  onClick,
}) => (
  <button
    className={classNames(
      styles.button,
      styles[variant],
      styles[size],
      ignoreSvgColor && styles.ignoreSvgColor,
      fullWidth && styles.fullWidth,
      destructive && styles.destructive,
      selected && styles.selected,
      disabled && styles.disabled,
      className
    )}
    type={htmlType}
    style={style}
    onClick={onClick}
  >
    {leadingIcon && <div className={styles.icon}>{leadingIcon}</div>}
    {children && (
      <Typography.Text variant={size.includes("small") ? "caption" : "body1"}>
        {children}
      </Typography.Text>
    )}
    {trailingIcon && <div className={styles.icon}>{trailingIcon}</div>}
  </button>
);

export { Button };
