import { ElementStyle } from "@utils";
import { ButtonHTMLAttributes, PropsWithChildren, ReactNode } from "react";

type ButtonVariant =
  | "primary"
  | "secondary"
  | "borders"
  | "no-borders"
  | "grey";
type ButtonSize =
  | "extra-large"
  | "large"
  | "medium"
  | "small"
  | "extra-small"
  | "x2-small";

interface ButtonProps extends PropsWithChildren, ElementStyle {
  variant: ButtonVariant;
  size: ButtonSize;
  fullWidth?: boolean;
  ignoreSvgColor?: boolean;
  selected?: boolean;
  destructive?: boolean;
  disabled?: boolean;
  leadingIcon?: ReactNode;
  trailingIcon?: ReactNode;
  htmlType?: ButtonHTMLAttributes<HTMLButtonElement>["type"];
  onClick?: () => void;
}

export type { ButtonProps };
