import type { Meta, StoryObj } from "@storybook/react";

import TestIcon from "@assets/icons/gear-filled.svg";
import { Button } from "./button.component";

const meta: Meta<typeof Button> = {
  title: "UI-KIT/Button",
  component: Button,
};

export const Default: StoryObj<typeof Button> = {
  args: {
    variant: "primary",
    size: "extra-large",
  },
  render: (args) => <Button {...args}>Submit</Button>,
};

export const WithIcon: StoryObj<typeof Button> = {
  args: {
    variant: "primary",
    size: "extra-large",
    leadingIcon: <TestIcon />,
    trailingIcon: <TestIcon />,
  },
  render: (args) => <Button {...args}>Submit</Button>,
};

export default meta;
