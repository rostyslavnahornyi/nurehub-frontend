import { FC, SVGProps } from "react";
import { LogoProps } from "./logo.types";

import BlueLogoIcon from "@assets/images/logo/logo-blue.svg";
import DarkLogoIcon from "@assets/images/logo/logo-dark.svg";
import GhostLogoIcon from "@assets/images/logo/logo-ghost.svg";
import LightLogoIcon from "@assets/images/logo/logo-light.svg";

const defineLogoSVGElement: (
  variant: LogoProps["variant"]
) => FC<SVGProps<SVGElement>> = (variant) => {
  switch (variant) {
    case "light":
      return LightLogoIcon;

    case "dark":
      return DarkLogoIcon;

    case "ghost":
      return GhostLogoIcon;

    case "blue":
      return BlueLogoIcon;
  }
};

const Logo: FC<LogoProps> = ({ variant, size = 32 }) => {
  const SVGElement = defineLogoSVGElement(variant);

  return <SVGElement style={{ width: size, height: size }} />;
};

export { Logo };
