type LogoProps = {
  size?: number;
  variant: "light" | "dark" | "ghost" | "blue";
};

export type { LogoProps };
