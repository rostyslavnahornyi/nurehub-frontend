import type { Meta, StoryObj } from "@storybook/react";

import { Logo } from "./logo.component";

const meta: Meta<typeof Logo> = {
  title: "UI-KIT/Logo",
  component: Logo,
};

export const Light: StoryObj<typeof Logo> = {
  args: {
    variant: "light",
    size: 64,
  },
};

export const Dark: StoryObj<typeof Logo> = {
  args: {
    variant: "dark",
    size: 64,
  },
};

export const Ghost: StoryObj<typeof Logo> = {
  args: {
    variant: "ghost",
    size: 64,
  },
};

export const Blue: StoryObj<typeof Logo> = {
  args: {
    variant: "blue",
    size: 64,
  },
};

export default meta;
