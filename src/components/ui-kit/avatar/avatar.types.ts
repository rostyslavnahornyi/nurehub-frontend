type AvatarSize = "2xl" | "xl" | "l" | "m" | "s" | "xs" | "2x" | "3x";

type CommonAvatarProps = {
  size: AvatarSize;
  initials?: string;
  stub?: boolean;
  src?: string;
};

export type { CommonAvatarProps };
