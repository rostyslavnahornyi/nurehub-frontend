import StubIcon from "@assets/icons/person.svg";
import { Typography } from "@components/ui-kit/typography";
import classNames from "classnames";
import { FC } from "react";
import styles from "./round-avatar.module.scss";
import { RoundAvatarProps } from "./round-avatar.types";

const RoundAvatar: FC<RoundAvatarProps> = ({
  size,
  initials,
  src,
  stub,
  className,
  ...props
}) => {
  if (!initials && !stub && !src) {
    throw new Error(
      'You should have ONE of the following props: "initials" | "src" | "stub"!'
    );
  }
  if ([!!initials, !!stub, !!src].filter(Boolean).length > 1) {
    throw new Error(
      'You should have ONLY ONE  avatar prop: "initials" | "src" | "stub"!'
    );
  }

  return (
    <div
      className={classNames(
        styles.roundAvatarWrapper,
        styles[`size-${size}`],
        src && styles.withImg,
        stub && styles.withStub,
        initials && styles.withInitials,
        className
      )}
      {...props}
    >
      {initials && (
        <Typography.Text variant="body1">{initials}</Typography.Text>
      )}
      {src && <img src={src} alt="avatar" />}
      {stub && <StubIcon />}
    </div>
  );
};

export { RoundAvatar };
