import { HTMLAttributes } from "react";
import { CommonAvatarProps } from "../../avatar.types";

interface RoundAvatarProps
  extends CommonAvatarProps,
    HTMLAttributes<HTMLDivElement> {}

export type { RoundAvatarProps };
