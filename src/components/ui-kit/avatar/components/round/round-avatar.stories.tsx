import FakeAvatar from "@assets/images/om-avatar.jpg";
import type { Meta, StoryObj } from "@storybook/react";

import { RoundAvatar } from "./round-avatar.component";

const meta: Meta<typeof RoundAvatar> = {
  title: "UI-KIT/Avatar",
  component: RoundAvatar,
};

export const Round: StoryObj<typeof RoundAvatar> = {
  args: { size: "2xl", src: FakeAvatar },
};

export default meta;
