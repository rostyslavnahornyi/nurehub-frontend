import { RoundAvatar } from "./components";

export { RoundAvatar } from "./components";

const Avatar = {
  Round: RoundAvatar,
};

export { Avatar };
