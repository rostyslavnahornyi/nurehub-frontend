import { DefaultInput, SelectInput, TagInput } from "./components";

const Input = {
  Default: DefaultInput,
  Select: SelectInput,
  Tag: TagInput,
};

export { Input };
