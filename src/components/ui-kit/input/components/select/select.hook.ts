import { DropdownOption } from "@components/ui-kit/dropdown/dropdown.types";
import { useClickOutside } from "@utils";
import { useEffect, useRef, useState } from "react";
import { usePopper } from "react-popper";
import { useDebounce } from "use-debounce";
import { SelectInputProps } from "./select.types";

const useSelect = ({
  options,
  defaultValue,
  onChange,
}: Pick<SelectInputProps, "defaultValue" | "options" | "onChange">) => {
  const [isDropdownOpened, setIsDropdownOpened] = useState(false);
  const [isFocused, setIsFocused] = useState(false);
  const [inputValue, setInputValue] = useState(defaultValue ?? "");
  const [currentOptions, setCurrentOptions] =
    useState<SelectInputProps["options"]>(options);
  const [selectedOption, setSelectedOption] = useState<DropdownOption>();

  const [referenceElement, setReferenceElement] =
    useState<HTMLDivElement | null>(null);
  const [popperElement, setPopperElement] = useState<HTMLDivElement | null>(
    null
  );
  const dropdownRef = useRef<HTMLUListElement>(null);
  const selectInputRef = useRef<HTMLDivElement>(null);
  const inputRef = useRef<HTMLInputElement>(null);

  const [debouncedValue] = useDebounce(inputValue, 250);

  const {
    styles: popperStyles,
    attributes,
    update,
  } = usePopper(referenceElement, popperElement, {
    placement: "bottom-start",
    modifiers: [
      {
        name: "offset",
        options: {
          offset: [0, 8],
        },
      },
      {
        name: "preventOverflow",
        options: {
          boundary: "clippingParents",
          padding: 0,
        },
      },
    ],
  });

  useEffect(() => {
    const foundOptions = options.filter(
      ({ label, value }) =>
        label.includes(debouncedValue) || value.includes(debouncedValue)
    );

    setCurrentOptions(foundOptions);
  }, [debouncedValue]);

  useEffect(() => {
    if (selectedOption) {
      setInputValue(selectedOption?.label);
      onChange(selectedOption.value);
    }
  }, [selectedOption]);

  useEffect(() => {
    if (!isDropdownOpened && isFocused) {
      setIsDropdownOpened(true);
    }
  }, [isFocused]);

  useEffect(() => {
    update && update();
  }, [isDropdownOpened]);

  useClickOutside(dropdownRef, () => setIsDropdownOpened(false));

  const removeAll = () => {
    setSelectedOption(undefined);
    setInputValue("");
    onChange(undefined);
  };

  const selectKeyDownHandler = () => {
    !isDropdownOpened && setIsDropdownOpened(true);
  };

  const onClickDropdown = (value: DropdownOption["value"]) => {
    setIsDropdownOpened(false);
    setSelectedOption(currentOptions.find((option) => value === option.value));
    selectInputRef.current?.focus();
  };

  return {
    inputRef,
    isFocused,
    inputValue,
    attributes,
    dropdownRef,
    popperStyles,
    selectInputRef,
    selectedOption,
    currentOptions,
    isDropdownOpened,
    removeAll,
    setIsFocused,
    setInputValue,
    onClickDropdown,
    setPopperElement,
    setReferenceElement,
    setIsDropdownOpened,
    selectKeyDownHandler,
  };
};

export { useSelect };
