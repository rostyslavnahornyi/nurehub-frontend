import ChevronDownIcon from "@assets/icons/chevron-down.svg";
import CrossIcon from "@assets/icons/x-03.svg";
import { Dropdown } from "@components/ui-kit/dropdown";
import { Portal } from "@components/ui-kit/portal";
import { Typography } from "@components/ui-kit/typography";
import classNames from "classnames";
import { FC, memo } from "react";
import { useSelect } from "./select.hook";
import styles from "./select.module.scss";
import { SelectInputProps } from "./select.types";

const SelectInputComp: FC<SelectInputProps> = ({
  title,
  placeholder,
  options,
  icon,
  clearable,
  defaultValue,
  fullWidth,
  searchable,
  onChange,
}) => {
  const {
    inputRef,
    isFocused,
    inputValue,
    attributes,
    dropdownRef,
    popperStyles,
    selectInputRef,
    selectedOption,
    currentOptions,
    isDropdownOpened,
    removeAll,
    setIsFocused,
    setInputValue,
    onClickDropdown,
    setPopperElement,
    setReferenceElement,
    setIsDropdownOpened,
    selectKeyDownHandler,
  } = useSelect({ options, defaultValue, onChange });

  return (
    <div
      className={classNames(
        styles.selectInputWrapper,
        fullWidth && styles.fullWidth
      )}
    >
      <div className={styles.selectInput} ref={setReferenceElement}>
        {title && <Typography.Text variant="body1">{title}</Typography.Text>}

        <div
          className={classNames(
            styles.bottomRow,
            isFocused && styles.focusedWrapper
          )}
          ref={selectInputRef}
        >
          {icon && <div className={styles.leftIconWrapper}>{icon}</div>}
          <input
            ref={inputRef}
            value={inputValue}
            readOnly={!searchable}
            className={styles.input}
            placeholder={placeholder}
            defaultValue={selectedOption?.label}
            onFocus={() => setIsFocused(true)}
            onBlur={() => setIsFocused(false)}
            onKeyDown={selectKeyDownHandler}
            onChange={({ currentTarget: { value } }) => setInputValue(value)}
          />
          <div className={styles.rightSideWrapper}>
            {clearable && (
              <>
                <CrossIcon onClick={removeAll} />
                <span className={styles.vertDivider} />
              </>
            )}

            <ChevronDownIcon
              onClick={() => setIsDropdownOpened(!isDropdownOpened)}
            />
          </div>
        </div>
      </div>

      <Portal id="dropdown">
        <div
          ref={setPopperElement}
          style={popperStyles.popper}
          {...attributes.popper}
        >
          <Dropdown
            style={{ width: selectInputRef?.current?.offsetWidth }}
            ref={dropdownRef}
            options={currentOptions}
            isOpened={isDropdownOpened}
            onClick={(value) => onClickDropdown(value)}
          />
        </div>
      </Portal>
    </div>
  );
};

const SelectInput = memo(SelectInputComp);

export { SelectInput };
