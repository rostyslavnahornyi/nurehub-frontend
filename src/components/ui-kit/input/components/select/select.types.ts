import { DropdownOption } from "@components/ui-kit/dropdown/dropdown.types";
import { ReactNode } from "react";

type SelectInputProps = {
  placeholder: string;
  options: DropdownOption[];
  title?: string;
  defaultValue?: DropdownOption["value"];
  clearable?: boolean;
  searchable?: boolean;
  disabled?: boolean;
  icon?: ReactNode;
  fullWidth?: boolean;
  /**
   * TODO
   */
  loading?: boolean;
  /**
   * TODO
   */
  multiple?: boolean;
  onChange: (value?: DropdownOption["value"]) => void;
};

export type { SelectInputProps };
