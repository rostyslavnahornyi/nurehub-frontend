import SearchIcon from "@assets/icons/search-01.svg";
import type { Meta, StoryObj } from "@storybook/react";

import { SelectInput } from "./select.component";

const meta: Meta<typeof SelectInput> = {
  title: "UI-KIT/Inputs/Select",
  component: SelectInput,
};

const OPTIONS = [
  { value: "chocolate", label: "Chocolate" },
  { value: "strawberry", label: "Strawberry" },
  { value: "vanilla", label: "Vanilla" },
];

export const Select: StoryObj<typeof SelectInput> = {
  args: {
    clearable: true,
    title: "Title",
    placeholder: "Placeholder",
    options: OPTIONS,
    fullWidth: false,
    icon: <SearchIcon />,
    onChange: (value) => console.log(value),
  },
};

export default meta;
