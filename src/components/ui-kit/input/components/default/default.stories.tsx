import type { Meta, StoryObj } from "@storybook/react";

import Icon from "@assets/icons/person.svg";
import { useState } from "react";
import { Button } from "../../../button";
import { DefaultInput } from "./default.component";

const meta: Meta<typeof DefaultInput> = {
  title: "UI-KIT/Inputs/Default",
  component: DefaultInput,
};

export const Default: StoryObj<typeof DefaultInput> = {
  args: {
    title: "Title",
    secondaryLabel: "Secondary Label",
    validationText: "Validation Text",
    placeholder: "Placeholder...",
    trailingIcon: <Icon />,
  },
  render: (props) => {
    const [error, setError] = useState<boolean>(false);

    return (
      <>
        <Button
          style={{ marginBottom: 10 }}
          size="medium"
          variant={"primary"}
          destructive={error}
          onClick={() => setError((err) => !err)}
        >
          Error: {error ? "True" : "False"}
        </Button>

        <DefaultInput
          {...props}
          validationText={"Some error"}
          onChange={() => null}
          error={error}
        />
      </>
    );
  },
};

export default meta;
