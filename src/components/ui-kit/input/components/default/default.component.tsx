import { Typography } from "@components/ui-kit";
import classNames from "classnames";
import { FC } from "react";
import { useDefaultInput } from "./default.hook";
import styles from "./default.module.scss";
import { DefaultInputProps } from "./default.types";

const DefaultInput: FC<DefaultInputProps> = ({
  size = "small",
  title,
  leadingIcon,
  trailingIcon,
  error = false,
  fullWidth = false,
  disabled = false,
  debounceMs = 0,
  secondaryLabel,
  validationText,
  defaultValue,
  onBlur,
  onFocus,
  onChange,
  ...props
}) => {
  const { isFocused, setIsFocused, setInputValue } = useDefaultInput({
    onChange,
    defaultValue,
    debounceMs,
  });

  return (
    <div
      className={classNames(
        styles.inputWrapper,
        styles[`size-${size}`],
        isFocused && styles.focusedWrapper,
        disabled && styles.disabledWrapper,
        error && styles.erroredWrapper,
        fullWidth && styles.fullWidthWrapper
      )}
    >
      <div className={styles.topRow}>
        <Typography.Text variant="body1">{title}</Typography.Text>
        {secondaryLabel && (
          <Typography.Text variant="body2">{secondaryLabel}</Typography.Text>
        )}
      </div>

      <div className={styles.innerInput}>
        {leadingIcon && <div className={styles.icon}>{leadingIcon}</div>}
        <input
          className={styles.input}
          onChange={({ target: { value } }) => setInputValue(value)}
          onFocus={() => {
            setIsFocused(true);
            onFocus && onFocus();
          }}
          onBlur={() => {
            setIsFocused(false);
            onBlur && onBlur();
          }}
          disabled={disabled}
          defaultValue={defaultValue}
          {...props}
        />

        {trailingIcon && <div className={styles.icon}>{trailingIcon}</div>}
      </div>

      <div className={styles.bottomRow}>
        {error && validationText && (
          <Typography.Text variant="body2">{validationText}</Typography.Text>
        )}
      </div>
    </div>
  );
};

export { DefaultInput };
