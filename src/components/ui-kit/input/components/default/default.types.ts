import { DetailedHTMLProps, InputHTMLAttributes, ReactNode } from "react";

interface DefaultInputProps
  extends Omit<
    DetailedHTMLProps<InputHTMLAttributes<HTMLInputElement>, HTMLInputElement>,
    "onChange" | "size"
  > {
  size?: "small" | "large";
  fullWidth?: boolean;
  leadingIcon?: ReactNode;
  trailingIcon?: ReactNode;
  defaultValue?: string;
  label?: string;
  error?: boolean;
  disabled?: boolean;
  debounceMs?: number;
  secondaryLabel?: string;
  validationText?: string;
  onFocus?: () => void;
  onBlur?: () => void;
  onChange: (value: string) => void;
}

export type { DefaultInputProps };
