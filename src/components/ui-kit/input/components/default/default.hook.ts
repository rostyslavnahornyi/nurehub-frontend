import { useEffect, useState } from "react";
import { useDebounce } from "use-debounce";
import { DefaultInputProps } from "./default.types";

const useDefaultInput = ({
  onChange,
  defaultValue,
  debounceMs = 0,
}: Pick<DefaultInputProps, "defaultValue" | "debounceMs" | "onChange">) => {
  const [inputValue, setInputValue] = useState(defaultValue ?? "");
  const [debouncedValue] = useDebounce(inputValue, debounceMs);
  const [isFocused, setIsFocused] = useState(false);

  useEffect(() => {
    onChange(debouncedValue);
  }, [debouncedValue]);

  return { isFocused, setIsFocused, setInputValue };
};

export { useDefaultInput };
