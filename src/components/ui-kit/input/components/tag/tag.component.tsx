import CrossIcon from "@assets/icons/x-03.svg";
import { Badge } from "@components/ui-kit/badge";
import { Typography } from "@components/ui-kit/typography";
import classNames from "classnames";
import { FC } from "react";
import { useTagInput } from "./tag.hook";
import styles from "./tag.module.scss";
import { TagInputProps } from "./tag.types";

const TagInput: FC<TagInputProps> = ({
  title,
  placeholder,
  secondaryTitle,
  onChange,
}) => {
  const {
    tags,
    inputRef,
    isFocused,
    inputValue,
    setIsFocused,
    setInputValue,
    onAddTagHandler,
    onRemoveTagHandler,
  } = useTagInput({ onChange });

  return (
    <div className={styles.tagInputWrapper}>
      {title && <Typography.Text variant="body1">{title}</Typography.Text>}

      <div
        className={classNames(
          styles.centerRow,
          isFocused && styles.focusedWrapper
        )}
      >
        <input
          type="text"
          placeholder={placeholder}
          value={inputValue}
          ref={inputRef}
          onFocus={() => {
            setIsFocused(true);
          }}
          onBlur={() => {
            setIsFocused(false);
          }}
          onChange={({ currentTarget: { value } }) => setInputValue(value)}
          onKeyDown={onAddTagHandler}
        />

        {tags.length ? (
          <div className={styles.badgesWrapper}>
            {tags.map((tag, idx) => (
              <Badge
                emphasis="low"
                size="extra-small"
                variant="ghost"
                key={idx}
                label={tag}
                icon={<CrossIcon onClick={() => onRemoveTagHandler(tag)} />}
              />
            ))}
          </div>
        ) : null}
      </div>

      {secondaryTitle && (
        <Typography.Text className={styles.secondaryTitle} variant="caption">
          {secondaryTitle}
        </Typography.Text>
      )}
    </div>
  );
};

export { TagInput };
