import { KeyboardEvent, useEffect, useRef, useState } from "react";
import { TagInputProps } from "./tag.types";

const useTagInput = ({ onChange }: Pick<TagInputProps, "onChange">) => {
  const [inputValue, setInputValue] = useState<string>("");
  const [tags, setTags] = useState<string[]>([]);
  const [isFocused, setIsFocused] = useState(false);

  const inputRef = useRef<HTMLInputElement>(null);

  useEffect(() => {
    onChange(tags.length ? tags : undefined);
  }, [tags]);

  const onAddTagHandler = (event: KeyboardEvent<HTMLInputElement>) => {
    if (event.key === "Enter" && inputValue) {
      event.preventDefault();
      setTags([...tags, inputValue]);
      setInputValue("");
    }
  };

  const onRemoveTagHandler = (value: string) => {
    const removedTagIndex = tags.findIndex((string) => string === value);

    if (removedTagIndex !== -1) {
      const newTags = tags.toSpliced(removedTagIndex, 1);
      setTags(newTags);
    }
  };

  return {
    tags,
    inputRef,
    isFocused,
    inputValue,
    setIsFocused,
    setInputValue,
    onAddTagHandler,
    onRemoveTagHandler,
  };
};

export { useTagInput };
