import type { Meta, StoryObj } from "@storybook/react";

import { TagInput } from "./tag.component";

const meta: Meta<typeof TagInput> = {
  title: "UI-KIT/Inputs/Tag",
  component: TagInput,
};

export const Tag: StoryObj<typeof TagInput> = {
  args: {
    title: "Title",
    placeholder: "Placeholder",
    secondaryTitle: "Secondary title",
    onChange: (value) => console.log(value),
  },
};

export default meta;
