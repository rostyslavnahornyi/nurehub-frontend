type TagInputProps = {
  title: string;
  placeholder: string;
  secondaryTitle?: string;
  onChange: (values?: string[]) => void;
};

export type { TagInputProps };
