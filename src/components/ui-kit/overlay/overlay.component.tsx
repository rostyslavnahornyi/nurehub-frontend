import { FC } from "react";
import { OverlayProps } from "./overlay.types";

import { motion } from "framer-motion";
import styles from "./overlay.module.scss";

const Overlay: FC<OverlayProps> = ({ children, onClick }) => (
  <motion.div
    className={styles.overlayWrapper}
    onClick={onClick}
    initial={{
      opacity: 0,
    }}
    animate={{
      opacity: 1,
      transition: { duration: 0.2 },
    }}
    exit={{
      opacity: 0,
      transition: { duration: 0.2 },
    }}
  >
    {children}
  </motion.div>
);

export { Overlay };
