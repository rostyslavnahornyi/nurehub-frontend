import { PropsWithChildren } from "react";

interface OverlayProps extends PropsWithChildren {
  onClick: () => void;
}

export type { OverlayProps };
