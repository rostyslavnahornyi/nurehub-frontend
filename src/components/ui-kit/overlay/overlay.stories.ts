import type { Meta, StoryObj } from "@storybook/react";

import { Overlay } from "./overlay.component";

const meta: Meta<typeof Overlay> = {
  title: "UI-KIT/Overlay",
  component: Overlay,
};

export const Misc: StoryObj<typeof Overlay> = {
  args: {},
};

export default meta;
