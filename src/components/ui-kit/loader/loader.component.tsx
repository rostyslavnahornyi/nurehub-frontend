import LoaderAltIcon from "@assets/icons/loader-alt.svg";
import classNames from "classnames";
import { FC } from "react";
import styles from "./loader.module.scss";
import { LoaderProps } from "./loader.types";

const Loader: FC<LoaderProps> = ({ size }) => (
  <LoaderAltIcon
    className={classNames(styles.loaderWrapper, styles[`size-${size}`])}
  />
);

export { Loader };
