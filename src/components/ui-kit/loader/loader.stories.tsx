import type { Meta, StoryObj } from "@storybook/react";

import { Loader } from "./loader.component";

const meta: Meta<typeof Loader> = {
  title: "UI-KIT/Loader",
  component: Loader,
};

export const Default: StoryObj<typeof Loader> = {
  args: {
    size: "large",
  },
};

export default meta;
