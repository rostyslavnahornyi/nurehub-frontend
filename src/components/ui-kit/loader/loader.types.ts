type LoaderProps = {
  size: "small" | "large";
};

export type { LoaderProps };
