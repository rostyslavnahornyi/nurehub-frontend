import ExampleIcon from "@assets/icons/eye.svg";
import type { Meta, StoryObj } from "@storybook/react";

import { Dropdown } from "./dropdown.component";
import { DropdownOption } from "./dropdown.types";

const meta: Meta<typeof Dropdown> = {
  title: "UI-KIT/Dropdown",
  component: Dropdown,
};

const OPTIONS: DropdownOption[] = [
  { value: "default", label: "Default", leadingIcon: <ExampleIcon /> },
  {
    value: "all_icons",
    label: "Icons",
    leadingIcon: <ExampleIcon />,
    trailingIcon: <ExampleIcon />,
  },
  {
    value: "selected",
    label: "Selected",
    leadingIcon: <ExampleIcon />,
  },
  {
    value: "disabled",
    label: "Disabled",
    leadingIcon: <ExampleIcon />,
    disabled: true,
  },
];

export const Default: StoryObj<typeof Dropdown> = {
  args: {
    defaultValue: "selected",
    isOpened: true,
    onClick: (value) => console.log(value),
    options: OPTIONS,
  },
};

export default meta;
