import { HTMLAttributes, ReactNode } from "react";

type DropdownOption = {
  label: string;
  value: string;
  disabled?: boolean;
  trailingIcon?: ReactNode;
  leadingIcon?: ReactNode;
  /**
   * TODO
   */
  badge?: null;
};

interface DropdownProps
  extends Pick<HTMLAttributes<HTMLUListElement>, "className" | "style"> {
  isOpened?: boolean;
  options: DropdownOption[];
  defaultValue?: DropdownOption["value"];
  rtl?: boolean;
  onClick: (value: DropdownOption["value"]) => void;
}

export type { DropdownOption, DropdownProps };
