import { useCallback, useEffect, useRef, useState } from "react";
import { useTranslation } from "react-i18next";
import { DropdownOption, DropdownProps } from "./dropdown.types";

const useDropdown = ({
  defaultValue,
  onClick,
}: Pick<DropdownProps, "defaultValue" | "onClick">) => {
  const { t } = useTranslation("components", { keyPrefix: "ui-kit.dropdown" });

  const [selectedValue, setSelectedValue] = useState<
    DropdownOption["value"] | null
  >(defaultValue ?? null);

  const ulRef = useRef<HTMLUListElement>(null);

  useEffect(() => {
    if (selectedValue) {
      onClick(selectedValue);
    }
  }, [selectedValue]);

  const isSelected = useCallback(
    (value: DropdownOption["value"]) => {
      if (selectedValue === value) return true;
      return false;
    },
    [selectedValue]
  );

  const onSelect = (value: DropdownOption["value"]) => {
    setSelectedValue(value);
  };

  return { t, ulRef, isSelected, onSelect };
};

export { useDropdown };
