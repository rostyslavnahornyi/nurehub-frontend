import classNames from "classnames";
import { forwardRef, useImperativeHandle } from "react";
import { Typography } from "../typography";
import { useDropdown } from "./dropdown.hook";
import styles from "./dropdown.module.scss";
import { DropdownProps } from "./dropdown.types";

const Dropdown = forwardRef<HTMLUListElement, DropdownProps>(
  (
    { options, isOpened, defaultValue, rtl, className, style, onClick },
    ref
  ) => {
    const { t, ulRef, isSelected, onSelect } = useDropdown({
      defaultValue,
      onClick,
    });

    useImperativeHandle(ref, () => ulRef.current as HTMLUListElement);

    return isOpened ? (
      <ul
        className={classNames(
          styles.dropdownWrapper,
          rtl && styles.rtl,
          className
        )}
        ref={ulRef}
        style={style}
      >
        {options.map(
          (
            { label, value, disabled, trailingIcon, leadingIcon, badge },
            index
          ) => (
            <li
              key={value}
              tabIndex={index}
              className={classNames(
                styles.optionWrapper,
                isSelected(value) && styles.selected,
                disabled && styles.disabled
              )}
              onClick={() => !disabled && onSelect(value)}
            >
              {leadingIcon && (
                <div className={styles.iconWrapper}>{leadingIcon}</div>
              )}
              {badge && <></>}

              <Typography.Text variant="body2">{label}</Typography.Text>

              {trailingIcon && (
                <div className={styles.iconWrapper}>{trailingIcon}</div>
              )}
            </li>
          )
        )}
        {!options.length && (
          <div className={styles.optionWrapper}>
            <Typography.Text variant="body2">{t("notFound")}</Typography.Text>
          </div>
        )}
      </ul>
    ) : null;
  }
);

export { Dropdown };
