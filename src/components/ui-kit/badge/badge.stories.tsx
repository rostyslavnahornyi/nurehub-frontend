import CrossIcon from "@assets/icons/x-03.svg";
import type { Meta, StoryObj } from "@storybook/react";
import { Badge } from "./badge.component";

const meta: Meta<typeof Badge> = {
  title: "UI-KIT/Badge",
  component: Badge,
};

export const Default: StoryObj<typeof Badge> = {
  args: {
    emphasis: "low",
    variant: "ghost",
    size: "2x-small",
    icon: <CrossIcon />,
    label: "Label",
    onClick: () => console.log("click"),
  },
};

export default meta;
