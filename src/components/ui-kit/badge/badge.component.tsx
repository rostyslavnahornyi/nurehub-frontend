import classNames from "classnames";
import { FC } from "react";
import { Typography } from "../typography";
import styles from "./badge.module.scss";
import { BadgeProps } from "./badge.types";

const Badge: FC<BadgeProps> = ({
  emphasis,
  variant,
  size,
  label,
  icon,
  onClick,
}) => (
  <button
    className={classNames(
      styles.badgeWrapper,
      styles[`emphasis--${emphasis}`],
      styles[`variant--${variant}`],
      styles[`size--${size}`],
      onClick && styles.isButton
    )}
    type="button"
    onClick={onClick}
  >
    {label && <Typography.Text variant="body1">{label}</Typography.Text>}
    {icon && <div className={styles.icon}>{icon}</div>}
  </button>
);

export { Badge };
