import { ReactNode } from "react";

type BadgeProps = {
  emphasis: "low" | "high";
  variant: "ghost" | "accent" | "error" | "warning" | "success" | "neutral";
  size: "2x-small" | "extra-small" | "small";
  label?: string;
  icon?: ReactNode;
  onClick?: () => void;
};

export type { BadgeProps };
