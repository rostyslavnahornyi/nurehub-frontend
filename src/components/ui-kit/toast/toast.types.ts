type ToastProps = {
  focusType: "high" | "medium" | "low";
  variant: "neutral" | "success" | "accent" | "warning" | "error" | "black";
  size: "small" | "medium" | "large";
  title: string;
  infoIcon?: boolean;
  primaryAction?: {
    onClick: () => void;
    label: string;
  };
  secondaryAction?: {
    onClick: () => void;
    label: string;
  };
  onClose?: () => void;
};

export type { ToastProps };
