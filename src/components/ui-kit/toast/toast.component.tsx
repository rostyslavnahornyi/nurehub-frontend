import InfoIcon from "@assets/icons/information-circle-contained-filled.svg";
import CloseIcon from "@assets/icons/x-03.svg";
import classNames from "classnames";
import { FC } from "react";
import { Typography } from "../typography";
import styles from "./toast.module.scss";
import { ToastProps } from "./toast.types";

const Toast: FC<ToastProps> = ({
  variant,
  focusType,
  size,
  title,
  infoIcon = true,
  primaryAction,
  secondaryAction,
  onClose,
}) => {
  return (
    <div
      className={classNames(styles.toastWrapper, styles[`variant-${variant}`])}
    >
      <div className={styles.header}>
        <div className={styles.leftSide}>
          {infoIcon && <InfoIcon className={styles.infoIcon} />}
          <Typography.Text variant="body1">{title}</Typography.Text>
        </div>
        <div className={styles.rightSide}>
          {primaryAction && (
            <Typography.Text variant="link2" onClick={primaryAction.onClick}>
              {primaryAction.label}
            </Typography.Text>
          )}

          {secondaryAction && (
            <Typography.Text variant="link2" onClick={secondaryAction.onClick}>
              {secondaryAction.label}
            </Typography.Text>
          )}

          {onClose && (
            <CloseIcon className={styles.closeIcon} onClick={onClose} />
          )}
        </div>
      </div>
      <div className={styles.body}>
        <div className={styles.actionButtons}></div>
      </div>
    </div>
  );
};

export { Toast };
