import type { Meta, StoryObj } from "@storybook/react";

import { Toast } from "./toast.component";

const meta: Meta<typeof Toast> = {
  title: "UI-KIT/Toast",
  component: Toast,
};

export const Misc: StoryObj<typeof Toast> = {
  args: {
    variant: "neutral",
    focusType: "high",
    size: "small",
    title: "Toast title",
    onClose: () => console.log("onClose handler"),
    primaryAction: {
      label: "Primary",
      onClick: () => console.log("onClick primary action handler"),
    },
    secondaryAction: {
      label: "secondary",
      onClick: () => console.log("onClick secondary action handler"),
    },
  },
};

export default meta;
