import type { Meta, StoryObj } from "@storybook/react";

import { Steps } from "./steps.component";

const meta: Meta<typeof Steps> = {
  title: "UI-KIT/Steps",
  component: Steps,
};

export const Misc: StoryObj<typeof Steps> = {
  args: {
    count: 4,
    currentIdx: 2,
  },
};

export default meta;
