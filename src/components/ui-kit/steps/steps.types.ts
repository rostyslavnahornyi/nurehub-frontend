import { HTMLAttributes } from "react";

interface StepsProps extends Pick<HTMLAttributes<HTMLDivElement>, "className"> {
  count: number;
  currentIdx: number;
}

export type { StepsProps };
