import classNames from "classnames";
import { FC } from "react";
import { Dot } from "../dot";
import styles from "./steps.module.scss";
import { StepsProps } from "./steps.types";

const Steps: FC<StepsProps> = ({ count, currentIdx, className }) => (
  <div className={classNames(styles.stepsWrapper, className)}>
    {Array(count)
      .fill(null)
      .map((_, idx) => (
        <Dot.Misc
          key={idx}
          size="large"
          color={currentIdx <= idx ? "grey" : "blue"}
        />
      ))}
  </div>
);

export { Steps };
