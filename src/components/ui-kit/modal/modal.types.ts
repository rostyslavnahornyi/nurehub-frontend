import { ModalIDs } from "@components/common/modals/components";
import { HTMLAttributes, PropsWithChildren } from "react";
import { ButtonProps } from "../button/button.types";

interface ModalProps
  extends PropsWithChildren,
    Pick<HTMLAttributes<HTMLDivElement>, "className"> {
  id: ModalIDs;
  title: string;
  size: "small" | "medium" | "large";
  controlButtons?: {
    primary: Omit<ButtonProps, "size">;
    secondary?: Omit<ButtonProps, "size">;
  };
  onPrevious?: () => void;
  onClose: () => void;
}

export type { ModalProps };
