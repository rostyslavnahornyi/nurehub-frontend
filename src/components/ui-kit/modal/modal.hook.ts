import { useEffect } from "react";
import { ModalProps } from "./modal.types";

const useModal = ({ onClose }: Pick<ModalProps, "onClose">) => {
  useEffect(() => {
    const accessabilityHandler = (e: globalThis.KeyboardEvent) => {
      switch (e.key) {
        case "Escape": {
          onClose();
        }
      }
    };

    document.body.style.overflow = "hidden";
    document.addEventListener("keydown", accessabilityHandler);

    return () => {
      document.body.style.overflow = "auto";
      document.removeEventListener("keydown", accessabilityHandler);
    };
  }, []);

  return {};
};

export { useModal };
