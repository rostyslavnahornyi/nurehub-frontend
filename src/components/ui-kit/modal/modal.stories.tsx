import type { Meta, StoryObj } from "@storybook/react";

import { useState } from "react";
import { Button } from "../button";
import { Modal } from "./modal.component";

const meta: Meta<typeof Modal> = {
  title: "UI-KIT/Modal",
  component: Modal,
};

export const Misc: StoryObj<typeof Modal> = {
  args: {
    id: "CONNECTION_ERROR",
    title: "Title",
    size: "small",
    controlButtons: {
      primary: { variant: "grey", children: "Primary" },
      secondary: { variant: "grey", children: "Secondary" },
    },
    onPrevious: () => console.log("onPrevious handler"),
  },
  render: (props) => {
    const [isVisible, setIsVisible] = useState(false);

    return (
      <div>
        <Button
          size="medium"
          variant="primary"
          onClick={() => setIsVisible((prev) => !prev)}
        >
          Open
        </Button>

        {isVisible && (
          <Modal {...props} onClose={() => setIsVisible(false)}>
            <p>For height</p>
            <p>For height</p>
            <p>For height</p>
            <p>For height</p>
            <p>For height</p>
            <p>For height</p>
            <p>For height</p>
            <p>For height</p>
            <p>For height</p>
            <p>For height</p>
            <p>For height</p>
            <p>For height</p>
            <p>For height</p>
            <p>For height</p>
            <p>For height</p>
            <p>For height</p>
            <p>For height</p>
            <p>For height</p>
            <p>For height</p>
            <p>For height</p>
            <p>For height</p>
            <p>For height</p>
            <p>For height</p>
            <p>For height</p>
            <p>For height</p>
            <p>For height</p>
            <p>For height</p>
            <p>For height</p>
            <p>For height</p>
            <p>For height</p>
            <p>For height</p>
            <p>For height</p>
            <p>For height</p>
            <p>For height</p>
            <p>For height</p>
            <p>For height</p>
            <p>For height</p>
            <p>For height</p>
            <p>For height</p>
            <p>For height</p>
            <p>For height</p>
            <p>For height</p>
            <p>For height</p>
            <p>For height</p>
          </Modal>
        )}
      </div>
    );
  },
};

export default meta;
