import LeftArrowIcon from "@assets/icons/arrow-left.svg";
import CrossIcon from "@assets/icons/x-03.svg";
import { default as classNames, default as classnames } from "classnames";
import { AnimatePresence, motion } from "framer-motion";
import { FC } from "react";
import { Button } from "../button";
import { Overlay } from "../overlay";
import { Portal } from "../portal";
import { Typography } from "../typography";
import { useModal } from "./modal.hook";
import styles from "./modal.module.scss";
import { ModalProps } from "./modal.types";

const Modal: FC<ModalProps> = ({
  id,
  size,
  title,
  controlButtons,
  children,
  className,
  onPrevious,
  onClose,
}) => {
  useModal({ onClose });

  return (
    <Portal id={`modal--${id}`}>
      <AnimatePresence>
        <Overlay onClick={onClose}>
          <motion.div
            className={styles.modalWrapper}
            initial={{
              opacity: 0,
              y: 30,
            }}
            animate={{
              opacity: 1,
              y: 0,
              transition: { duration: 0.2 },
            }}
            exit={{
              opacity: 0,
              y: 30,
              transition: { duration: 0.2 },
            }}
          >
            <motion.div className={styles.modalContainer}>
              <div
                className={classnames(styles.modal, styles[`size-${size}`])}
                onClick={(e) => {
                  e.stopPropagation();
                }}
              >
                <div className={styles.header}>
                  <div className={styles.leftSide}>
                    {onPrevious && (
                      <Button
                        size="medium"
                        variant="no-borders"
                        trailingIcon={<LeftArrowIcon />}
                        onClick={onPrevious}
                      />
                    )}
                    <Typography.Text variant="body1">{title}</Typography.Text>
                  </div>
                  <CrossIcon onClick={onClose} />
                </div>

                <div className={classNames(styles.inner, className)}>
                  {children}
                </div>

                {controlButtons && (
                  <div className={styles.footer}>
                    <Button
                      {...controlButtons.primary}
                      size="medium"
                      fullWidth
                    />
                    {controlButtons.secondary && (
                      <Button
                        {...controlButtons.secondary}
                        size="medium"
                        fullWidth
                      />
                    )}
                  </div>
                )}
              </div>
            </motion.div>
          </motion.div>
        </Overlay>
      </AnimatePresence>
    </Portal>
  );
};

export { Modal };
