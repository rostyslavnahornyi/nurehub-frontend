import type { Meta, StoryObj } from "@storybook/react";

import { Divider } from "./divider.component";

const meta: Meta<typeof Divider> = {
  title: "UI-KIT/Divider",
  component: Divider,
};

export const Default: StoryObj<typeof Divider> = {};

export const WithText: StoryObj<typeof Divider> = {
  args: {
    textProps: { variant: "body1", children: "Text" },
  },
};

export const WithButton: StoryObj<typeof Divider> = {
  args: {
    buttonProps: {
      size: "extra-large",
      variant: "grey",
      children: "Stub",
    },
  },
};

export default meta;
