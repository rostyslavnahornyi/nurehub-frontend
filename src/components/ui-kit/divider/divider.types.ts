import { TextProps } from "@components/ui-kit/typography/text/text.types";
import { ButtonProps } from "../button/button.types";

type DividerProps = {
  textProps?: TextProps;
  buttonProps?: ButtonProps;
};

export type { DividerProps };
