import { FC } from "react";
import { Button } from "../button";
import { Typography } from "../typography";
import styles from "./divider.module.scss";
import { DividerProps } from "./divider.types";

const Divider: FC<DividerProps> = ({ buttonProps, textProps }) =>
  buttonProps || textProps ? (
    <div className={styles.dividerWrapper}>
      <hr className={styles.hr} />
      {buttonProps && <Button {...buttonProps} />}
      {textProps && <Typography.Text {...textProps} />}
      <hr className={styles.hr} />
    </div>
  ) : (
    <hr className={styles.hr} />
  );

export { Divider };
