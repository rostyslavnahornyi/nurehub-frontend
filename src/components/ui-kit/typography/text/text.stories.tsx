import type { Meta, StoryObj } from "@storybook/react";

import { Text } from "./text.component";

const meta: Meta<typeof Text> = {
  title: "UI-KIT/Typography/Text",
  component: Text,
};

export const Base: StoryObj<typeof Text> = {
  render: () => (
    <div
      style={{
        width: 300,
      }}
    >
      <Text variant="h1">H1</Text>
      <Text variant="h2">H2</Text>
      <Text variant="h3">H3</Text>
      <Text variant="h4">H4</Text>
      <Text variant="h5">H5</Text>
      <Text variant="h6">H6</Text>
      <Text variant="body1">Body 1</Text>
      <Text variant="body2">Body 2</Text>
      <Text variant="caption">Caption</Text>
      <Text variant="link1">Link 1</Text>
      <br />
      <Text variant="link2">Link 2</Text>
    </div>
  ),
};

export default meta;
