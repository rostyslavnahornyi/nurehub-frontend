import { ElementStyle } from "@utils";
import { PropsWithChildren } from "react";

interface TextProps extends PropsWithChildren, ElementStyle {
  variant:
    | "h1"
    | "h2"
    | "h3"
    | "h4"
    | "h5"
    | "h6"
    | "body1"
    | "body2"
    | "link1"
    | "link2"
    | "caption";
  onClick?: () => void;
}

export type { TextProps };
