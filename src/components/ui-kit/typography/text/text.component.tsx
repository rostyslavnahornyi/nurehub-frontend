import classnames from "classnames";
import { FC } from "react";
import styles from "./text.module.scss";
import { TextProps } from "./text.types";

const defineElement: (
  variant: TextProps["variant"]
) => "p" | "a" | "h1" | "h2" | "h3" | "h4" | "h5" | "h6" = (variant) => {
  switch (variant) {
    case "body1":
    case "body2":
    case "caption":
      return "p";

    case "link1":
    case "link2":
      return "a";

    default:
      return variant;
  }
};

const Text: FC<TextProps> = ({
  variant,
  children,
  className,
  style,
  onClick,
}) => {
  const Element = defineElement(variant);

  return (
    <Element
      style={style}
      className={classnames(styles[variant], className)}
      onClick={onClick}
    >
      {children}
    </Element>
  );
};

export { Text };
