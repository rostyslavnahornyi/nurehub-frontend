import { HTMLAttributes } from "react";

interface MiscDotProps extends HTMLAttributes<HTMLDivElement> {
  color: "blue" | "white" | "black" | "green" | "grey";
  size: "small" | "large";
}

export type { MiscDotProps };
