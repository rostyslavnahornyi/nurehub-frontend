import classNames from "classnames";
import { FC } from "react";
import styles from "./misc-dot.module.scss";
import { MiscDotProps } from "./misc-dot.types";

const MiscDot: FC<MiscDotProps> = ({ color, size, className, ...props }) => (
  <div
    className={classNames(
      styles.miscDotWrapper,
      styles[`color-${color}`],
      styles[`size-${size}`],
      className
    )}
    {...props}
  />
);

export { MiscDot };
