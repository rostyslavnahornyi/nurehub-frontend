import type { Meta, StoryObj } from "@storybook/react";

import { MiscDot } from "./misc-dot.component";

const meta: Meta<typeof MiscDot> = {
  title: "UI-KIT/Dots",
  component: MiscDot,
};

export const Misc: StoryObj<typeof MiscDot> = {
  args: {
    size: "small",
    color: "black",
  },
};

export default meta;
